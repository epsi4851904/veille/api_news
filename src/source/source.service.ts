import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Source, Prisma } from '@prisma/client';

@Injectable()
export class SourceService {
    constructor(private prisma: PrismaService) {}

    async create(data: Prisma.SourceCreateInput): Promise<Source> {
        return this.prisma.source.create({ data });
    }

    async findAll(): Promise<Source[]> {
        return this.prisma.source.findMany();
    }

    async findOne(where: Prisma.SourceWhereUniqueInput): Promise<Source | null> {
        return this.prisma.source.findUnique({ where });
    }
}
