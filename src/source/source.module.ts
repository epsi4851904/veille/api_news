import { Module } from '@nestjs/common';
import { SourceService } from './source.service';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
    imports: [PrismaModule],
    providers: [SourceService],
    exports: [SourceService],
})
export class SourceModule {}
