import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { News, Prisma } from '@prisma/client';

@Injectable()
export class NewsService {
    constructor(private prisma: PrismaService) {}

    async create(data: Prisma.NewsCreateInput): Promise<News> {
        return this.prisma.news.create({ data });
    }

    async findAll(): Promise<News[]> {
        return this.prisma.news.findMany({
            orderBy: {
                createdAt: 'desc',
            },
            include: {

            }
        });
    }

    async findOne(where: Prisma.NewsWhereUniqueInput, userId?: string): Promise<News | null> {
        const includeUserNews = userId ? { User_News: true } : {};
        return this.prisma.news.findUnique({
            where,
            include: {
                comments: {
                    include: {
                        User: true
                    }
                },
                ...includeUserNews
            },
        });
    }

    async update(params: {
        where: Prisma.NewsWhereUniqueInput;
        data: Prisma.NewsUpdateInput;
    }): Promise<News> {
        const { where, data } = params;
        return this.prisma.news.update({ where, data });
    }

    async findMany(where: Prisma.NewsWhereInput): Promise<News[]> {
        return this.prisma.news.findMany({ where });
    }

    async getTopLikedNews(): Promise<News[]> {
        return this.prisma.news.findMany({
            orderBy: {
                User_News: {
                    _count: 'desc',
                },
            },
            take: 5
        });
    }
}
