import { Module } from '@nestjs/common';
import { UserNewsService } from './user_news.service';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  providers: [UserNewsService],
  exports: [UserNewsService],
})
export class UserNewsModule {}
