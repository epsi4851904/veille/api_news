import { Test, TestingModule } from '@nestjs/testing';
import { UserNewsService } from './user_news.service';

describe('UserNewsService', () => {
  let service: UserNewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserNewsService],
    }).compile();

    service = module.get<UserNewsService>(UserNewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
