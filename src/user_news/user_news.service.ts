import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import {User_News, Prisma, News} from '@prisma/client';

@Injectable()
export class UserNewsService {
    constructor(private prisma: PrismaService) {}

    async likeNews(newsId: string, userId: string): Promise<User_News> {
        const existingUserNews = await this.prisma.user_News.findFirst({
            where: {
                User_Id: userId,
                News_id: newsId,
            },
        });

        if (existingUserNews) {
            return this.prisma.user_News.update({
                where: {
                    User_News_id: existingUserNews.User_News_id,
                },
                data: {
                    liked: !existingUserNews.liked,
                },
            });
        } else {
            return this.prisma.user_News.create({
                data: {
                    User_Id: userId,
                    News_id: newsId,
                    liked: true,
                },
            });
        }
    }

    async getLikedNewsByUser(userId: string): Promise<News[]> {
        return this.prisma.news.findMany({
            where: {
                User_News: {
                    some: {
                        User_Id: userId,
                        liked: true,
                    },
                },
            },
        })
    }
    async fakeNews(newsId: Prisma.User_NewsWhereUniqueInput, description: string, userId: string): Promise<User_News> {
        return this.prisma.user_News.update({
            where: newsId,
            data: {
                User_Id: userId,
                fake: !newsId.fake,
                description: description,
            }
        });
    }

    async saveNews(newsId: Prisma.User_NewsWhereUniqueInput): Promise<User_News> {
        return this.prisma.user_News.update({
            where: newsId,
            data: {
                pined: !newsId.pined
            }
        });
    }


}
