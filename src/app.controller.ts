
import {
  Controller,
  Get,
  Post,
  Param,
  Request,
  Query,
  Body,
  UseGuards,
  Header,
  Put, Delete,
} from '@nestjs/common';
import {ApiBearerAuth, ApiParam, ApiOperation, ApiQuery, ApiBody, ApiHeader, ApiTags} from '@nestjs/swagger';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { NewsService } from './news/news.service';
import { UserNewsService } from './user_news/user_news.service';
import { UserService } from './user/user.service';
import { FluxService } from './flux/flux.service';
import { SourceService } from './source/source.service';
import { AuthGuard } from './auth/auth.guard';
import {User, News, User_News, Flux, Comment} from '@prisma/client';
import {CommentService} from "./comment/comment.service";

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly newsService: NewsService,
    private readonly fluxService: FluxService,
    private readonly sourceService: SourceService,
    private readonly userNewsService: UserNewsService,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly commentService: CommentService
  ) {}

  @Post('user/create')
  @ApiOperation({ summary: 'Create a new user' })
  @ApiBody({ schema: { type: 'object', properties: { username: { type: 'string' }, email: { type: 'string' }, password: { type: 'string' } } } })
  @ApiTags('Registration')
  async createUser(
    @Body('username') username: string,
    @Body('email') email: string,
    @Body('password') password: string,
    @Body('isAdmin') isAdmin?: boolean
  ): Promise<User> {
    return this.userService.create({
      username,
      email,
      password,
      isAdmin
    });
  }

  @Post('user/login')
  @ApiTags('Login')
  @ApiOperation({ summary: 'Login as a user' })
  @ApiBody({ schema: { type: 'object', properties: { email: { type: 'string' }, password: { type: 'string' } } } })
  async loginUser(
    @Body('email') email: string,
    @Body('password') password: string,
  ): Promise<any | null> {
    const user = await this.authService.validateUser(email, password);
    if (!user) {
      return { message: 'Invalid username or password', code: 404 };
    }
    return { 'Bearer': await this.authService.login(user)}
  }

  @Get('user/me')
  @ApiTags('User')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Get the current user' })
  async getCurrentUser(@Request() req): Promise<User | null> {
    return this.userService.findOne({ User_Id: req.user.User_Id });
  }

  @Get('/users')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiTags('User')
  @ApiOperation({ summary: 'Get all users' })
  async getUsers(): Promise<User[]>{
    return this.userService.findAdmin({ isAdmin: true })
  }

  @Put('/user/me')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiBody({ schema: { type: 'object', properties: { email: { type: 'string' }, username: { type: 'string' } } } })
  @ApiTags('User')
  async updateUser(
      @Request() req,
      @Body() data: {username: string, email: string}
  ): Promise<User> {
    return this.userService.update({ User_Id: req.user.User_Id }, data);
  }

  @Delete('/user/:id')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiParam({ name: 'id', required: true })
  @ApiBody({ schema: { type: 'object', properties: { email: { type: 'string' }, username: { type: 'string' } } } })
  @ApiTags('User')
  async deleteUser(
      @Param('id') id: string
  ): Promise<User> {
    return this.userService.delete({ User_Id: id });
  }

  @Post('flux/create')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Create a new flux' })
  @ApiTags('Flux')
  @ApiBody({ schema: { type: 'object', properties: { type: { type: 'string' }, flux_url: { type: 'string' }, name: { type: 'string' }, alert_array: { type: 'string' }, alert: { type: 'string'}, alert_data: { type: 'string[]' } } } })
  async createFlux(
    @Body('type') type: string,
    @Body('flux_url') flux_url: string,
    @Body('name') name: string,
    @Body('alert_array') alert_array: string,
    @Body('alert') alert: string,
    @Body('alert_data') alert_data: string[]
  ): Promise<any> {
    return this.fluxService.create({
      type,
      flux_url,
      name,
      alert_array,
      alert,
      alert_data,
    });
  }

  @Get('flux')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Get all fluxes' })
  @ApiTags('Flux')
  async getFlux(): Promise<Flux[]> {
    return this.fluxService.findAll();
  }

  @Get('flux/:id')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Get a flux by ID' })
  @ApiParam({ name: 'id', required: true })
  @ApiTags('Flux')
  async getFluxById(@Param('id') id: string): Promise<Flux | null> {
    return this.fluxService.findOne({ Flux_id: id });
  }

  @Post('source/create')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Create a new source' })
  @ApiQuery({ name: 'name', required: true })
  @ApiQuery({ name: 'url', required: true })
  @ApiQuery({ name: 'main_div', required: true })
  async createSource(
    @Query('name') name: string,
    @Query('url') url: string,
    @Query('main_div') main_div: string
  ): Promise<any> {
    return this.sourceService.create({
      name,
      url,
      main_div,
    });
  }

  @Get('source')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Get all sources' })
  async getSource(): Promise<any> {
    return this.sourceService.findAll();
  }

  

  @Post('news/create')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token') 
  @ApiOperation({ summary: 'Create a new news article' })
  @ApiQuery({ name: 'title', required: true })
  @ApiQuery({ name: 'text', required: true })
  @ApiQuery({ name: 'description', required: true })
  @ApiQuery({ name: 'sourceURL', required: true })
  @ApiQuery({ name: 'pictureURL', required: true })
  @ApiQuery({ name: 'type', required: true })
  @ApiTags('News')
  async createNews(
    @Body() body: {
      flux_id: string,
      title: string,
      text: string[],
      description: string,
      sourceURL: string,
      pictureURL: string,
      type: string
    }
  ): Promise<News> {
    const { flux_id, title, text, description, sourceURL, pictureURL, type } = body;
    return this.newsService.create({
      Flux: { connect: { Flux_id: flux_id } },
      title,
      text,
      description,
      sourceURL,
      pictureURL,
      type,
      view_number: 0,
      like_number: 0
    });
  }

  @Put('news/update')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Update a news article' })
  @ApiQuery({ name: 'news_id', required: true })
  @ApiQuery({ name: 'text', required: false })
  @ApiQuery({ name: 'description', required: false })
  @ApiQuery({ name: 'pictureURL', required: false })
  @ApiQuery({ name: 'type', required: false })
  async updateNews(
    @Body() body: {
      news_id: string,
      text: string[],
      description: string,
      pictureURL: string,
      type: string
    }
  ): Promise<News> {
    const { news_id, text, description, pictureURL, type } = body;
    return this.newsService.update({
      where: { News_id: news_id },
      data: {
        text,
        description,
        pictureURL,
        type
      },
    });
  }

  @Get('news')
  @ApiOperation({ summary: 'Get all news articles' })
  @ApiTags('News')
  async getNews(): Promise<News[]> {
    return this.newsService.findAll();
  }

  @Get('news/:id')
  @ApiOperation({ summary: 'Get a news article by ID' })
  @ApiParam({ name: 'id', required: true })
  @ApiTags('News')
  async getNewsById(
      @Request() req,
      @Param('id') id: string
  ): Promise<News | null> {
    const user = req.user ? req.user.User_Id : null
    return this.newsService.findOne({ News_id: id }, user);
  }

  @Get('news/flux/:id')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Get all news articles from a flux' })
  @ApiParam({ name: 'id', required: true })
  @ApiTags('Flux')
  async getNewsByFlux(@Param('id') id: string): Promise<{data: News[], total_rows: number}> {
    const data = await this.newsService.findMany({ Flux_id: id });
    return {
      data: data,
      total_rows: data.length
    }
  }

  @Post('news/like/:newsId')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token') 
  @ApiOperation({ summary: 'Like or Dislike a news article' })
  @ApiTags('News')
  async likeNews(
      @Param('newsId') newsId: any,
      @Request() req
  ): Promise<User_News> {
    return this.userNewsService.likeNews(newsId, req.user.User_Id);
  }

  @Post('news/fake')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token') 
  @ApiOperation({ summary: 'Mark a news article as fake' })
  @ApiQuery({ name: 'user_id', required: true })
  @ApiQuery({ name: 'news_id', required: true })
  @ApiQuery({ name: 'description', required: true })
  @ApiTags('News')
  async fakeNews(
    @Query('user_id') user_id: string,
    @Query('news_id') news_id: any,
    @Query('description') description: string,
  ): Promise<User_News> {
    return this.userNewsService.fakeNews(news_id, user_id, description);
  }

  @Post(':newsId/comments/create')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Create a comment on a news article' })
  @ApiBody({ schema: { type: 'object', properties: { text: { type: 'string' }, id_comment_replay: { type: 'string', nullable: true } } } })
  @ApiTags('Comments')
  async createComment(
      @Param('newsId') newsId: string,
      @Request() req,
      @Body('text') text: string,
      @Body('id_comment_replay') id_comment_replay?: string,
  ): Promise<Comment> {
    return this.commentService.create({
      text,
      User: { connect: { User_Id: req.user.User_Id } },
      News: { connect: { News_id: newsId } },
      Comment: id_comment_replay ? { connect: { Comment_id: id_comment_replay } } : undefined,
    });
  }

  @Get(':newsId/comments')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Get a flux by ID' })
  @ApiParam({ name: 'newsId', required: true })
  @ApiTags('Comments')
  async getCommentByNews(
      @Param('newsId') newsId: string
  ): Promise<Comment[] | null> {
    return this.commentService.findMany({ News_id: newsId });
  }

  @Get('liked')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('access-token')
  @ApiOperation({ summary: 'Like or Dislike a news article' })
  @ApiTags('News')
  async getLikedNews(@Request() req): Promise<News[]> {
    return this.userNewsService.getLikedNewsByUser(req.user.User_Id);
  }

  @Get('top-liked')
  @ApiOperation({ summary: 'Get the top 10 most liked news articles' })
  @ApiTags('News')
  async getTopLikedNews(): Promise<News[]> {
    return this.newsService.getTopLikedNews();
  }
}
