import { Module } from '@nestjs/common';
import { FluxService } from './flux.service';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
    imports: [PrismaModule],
    providers: [FluxService],
    exports: [FluxService],
})
export class FluxModule {}
