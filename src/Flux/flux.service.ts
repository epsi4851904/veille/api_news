import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Flux, Prisma } from '@prisma/client';

@Injectable()
export class FluxService {
    constructor(private prisma: PrismaService) {}

    async create(data: Prisma.FluxCreateInput): Promise<Flux> {
        return this.prisma.flux.create({ data });
    }

    async findAll(): Promise<Flux[]> {
        return this.prisma.flux.findMany();
    }

    async findOne(where: Prisma.FluxWhereUniqueInput): Promise<Flux | null> {
        return this.prisma.flux.findUnique({ where });
    }

}
