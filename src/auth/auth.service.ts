import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.findOne({ email });
    if (user && await bcrypt.compare(password, user.password)) {
      return {
        id: user.User_Id,
        username: user.username,
      };
    }
    return null;
  }

  async validateUserById(id: string): Promise<any> {
    return this.userService.findOne({ User_Id: id });
  }

  async login(user: any) {
    const payload = { username: user.username, id: user.id };
    console.log('Payload:', payload);
    return this.jwtService.signAsync(payload);
  }
}
