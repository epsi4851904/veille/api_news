import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { NewsModule } from './news/news.module';
import { UserNewsModule } from './user_news/user_news.module';
import { PrismaModule } from './prisma/prisma.module';
import { AuthModule } from './auth/auth.module';
import { FluxService } from './flux/flux.service';
import { FluxModule } from './flux/flux.module';
import { SourceService } from './source/source.service';
import { SourceModule } from './source/source.module';
import {CommentModule} from "./comment/comment.module";

@Module({
  imports: [ PrismaModule, UserModule, NewsModule, UserNewsModule, AuthModule, FluxModule, SourceModule, CommentModule],
  controllers: [AppController],
  providers: [AppService, FluxService, SourceService],
})
export class AppModule {}
