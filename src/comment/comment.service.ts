import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import {Comment, News, Prisma} from '@prisma/client';

@Injectable()
export class CommentService {
    constructor(private prisma: PrismaService) {}

    async create(data: Prisma.CommentCreateInput): Promise<Comment> {
        return this.prisma.comment.create({ data });
    }

    async findMany(where: Prisma.CommentWhereInput): Promise<Comment[]> {
        return this.prisma.comment.findMany({ where });
    }
}
