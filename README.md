## Install
$ npm install

# Running the api

$ npm run start

# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov

## The AI will be upgrade soon

## To use swagger just go to localhost:3000/api, create an account or log you in and add your token to the Authorize on the top right