-- CreateTable
CREATE TABLE "User" (
    "User_Id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedAt" TIMESTAMP(3),
    "deletingAt" TIMESTAMP(3),

    CONSTRAINT "User_pkey" PRIMARY KEY ("User_Id")
);

-- CreateTable
CREATE TABLE "News" (
    "News_id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "text" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "sourceURL" TEXT NOT NULL,
    "pictureURL" TEXT NOT NULL,
    "view_number" INTEGER NOT NULL,
    "like_number" INTEGER NOT NULL,
    "type" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedAt" TIMESTAMP(3),
    "deletingAt" TIMESTAMP(3),

    CONSTRAINT "News_pkey" PRIMARY KEY ("News_id")
);

-- CreateTable
CREATE TABLE "User_News" (
    "User_News_id" TEXT NOT NULL,
    "User_Id" TEXT NOT NULL,
    "News_id" TEXT NOT NULL,
    "liked" BOOLEAN NOT NULL,
    "fake" BOOLEAN,
    "description" TEXT,
    "pined" BOOLEAN,

    CONSTRAINT "User_News_pkey" PRIMARY KEY ("User_News_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- AddForeignKey
ALTER TABLE "User_News" ADD CONSTRAINT "User_News_User_Id_fkey" FOREIGN KEY ("User_Id") REFERENCES "User"("User_Id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "User_News" ADD CONSTRAINT "User_News_News_id_fkey" FOREIGN KEY ("News_id") REFERENCES "News"("News_id") ON DELETE RESTRICT ON UPDATE CASCADE;
