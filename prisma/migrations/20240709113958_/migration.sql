-- CreateTable
CREATE TABLE "Comment" (
    "Comment_id" TEXT NOT NULL,
    "text" TEXT NOT NULL,
    "published_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "User_Id" TEXT NOT NULL,
    "News_id" TEXT NOT NULL,
    "id_comment_replay" TEXT,

    CONSTRAINT "Comment_pkey" PRIMARY KEY ("Comment_id")
);

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_User_Id_fkey" FOREIGN KEY ("User_Id") REFERENCES "User"("User_Id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_News_id_fkey" FOREIGN KEY ("News_id") REFERENCES "News"("News_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_id_comment_replay_fkey" FOREIGN KEY ("id_comment_replay") REFERENCES "Comment"("Comment_id") ON DELETE SET NULL ON UPDATE CASCADE;
