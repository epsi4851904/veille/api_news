/*
  Warnings:

  - You are about to drop the column `Source_id` on the `News` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "News" DROP CONSTRAINT "News_Source_id_fkey";

-- AlterTable
ALTER TABLE "News" DROP COLUMN "Source_id";
