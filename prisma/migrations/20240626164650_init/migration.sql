/*
  Warnings:

  - Added the required column `Source_id` to the `News` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "News" ADD COLUMN     "Source_id" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "Source" (
    "Source_id" TEXT NOT NULL,
    "main_div" TEXT NOT NULL,

    CONSTRAINT "Source_pkey" PRIMARY KEY ("Source_id")
);

-- AddForeignKey
ALTER TABLE "News" ADD CONSTRAINT "News_Source_id_fkey" FOREIGN KEY ("Source_id") REFERENCES "Source"("Source_id") ON DELETE RESTRICT ON UPDATE CASCADE;
