/*
  Warnings:

  - Added the required column `Flux_id` to the `News` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "User_username_key";

-- AlterTable
ALTER TABLE "News" ADD COLUMN     "Flux_id" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "Flux" (
    "Flux_id" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "flux_url" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "alert_array" TEXT NOT NULL,
    "alert" TEXT NOT NULL,
    "alert_data" TEXT[],
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedAt" TIMESTAMP(3),
    "deletingAt" TIMESTAMP(3),

    CONSTRAINT "Flux_pkey" PRIMARY KEY ("Flux_id")
);

-- AddForeignKey
ALTER TABLE "News" ADD CONSTRAINT "News_Flux_id_fkey" FOREIGN KEY ("Flux_id") REFERENCES "Flux"("Flux_id") ON DELETE RESTRICT ON UPDATE CASCADE;
