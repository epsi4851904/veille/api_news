import requests
from dotenv import load_dotenv
from bs4 import BeautifulSoup
import os
import xml.etree.ElementTree as ET
import feedparser
import urllib.parse
from langdetect import detect
from transformers import pipeline

def load_env_variables():
    load_dotenv()
    username = os.getenv("API_USERNAME")
    password = os.getenv("API_PASSWORD")
    api_url = os.getenv("API_URL")
    
    return username, password, api_url

def login(api_url, username, password):
    url = f"{api_url}/user/login"
    payload = {
        "email": username,
        "password": password
    }

    try:
        response = requests.post(url, json=payload)
        response.raise_for_status()
        if response.status_code == 201:
            print("Login successful!")
            jwt = response.json().get("Bearer")
            if jwt:
                return jwt
            else:
                raise ValueError("JWT token not found in the response.")
        else:
            print(f"Login failed! Status code: {response.status_code}")
            return None
    except requests.exceptions.RequestException as e:
        print(f"An error occurred during login: {e}")
        return None

def get_flux_data(api_url, jwt):
    url = f"{api_url}/flux"
    headers = {"Authorization": f"Bearer {jwt}"}
    
    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        if response.status_code == 200:
            print("Flux data retrieved successfully!")
            flux = response.json()
            return flux
        else:
            print(f"Flux data retrieval failed! Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while retrieving flux data: {e}")
        
def get_news_data(flux, jwt, api_url):
    for item in flux:
        if item.get("flux_url"):
            url = item["flux_url"]
            try:
                headers = {
                    'User-Agent': 'Popular browser\'s user-agent',
                }
                response = requests.get(url, headers=headers)
                response.raise_for_status()
                if response.status_code == 200:
                    print("News data retrieved successfully!")
                    parse_xml(response.content, item["alert_array"], item["alert"], item["alert_data"], item["Flux_id"], jwt, api_url)
                else:
                    print(f"News data retrieval failed! Status code: {response.status_code}")
            except requests.exceptions.RequestException as e:
                print(f"An error occurred while retrieving news data: {e}")

def parse_xml(xml_content, alert_array, alert, alert_data, flux_id, jwt, api_url):
    try:
        if alert_array == "feed":
            feed = feedparser.parse(xml_content)
            for entry in feed.entries:
                item_data = {}
                item_data["flux_id"] = flux_id
                for data_field in alert_data:
                    if data_field in entry:
                        item_data[data_field] = entry[data_field]
                    else:
                        item_data[data_field] = None
                try:
                    lang = check_lang(item_data["title"])
                    if lang == "en" or lang == "fr":
                        print(item_data["link"])
                        if verify_news_is_not_already_posted(flux_id, item_data["title"], jwt, api_url):
                            sources = get_source_data(api_url, jwt)
                            source = check_url_source(sources, item_data["link"])
                            result = zero_shot_classify(item_data["title"], ["artificial intelligence", "machine learning", "deep learning", "neural networks", "ai", "software development", "programming", "coding", "web development", "software engineering", "developer", "code"])
                            if source is not None and result["scores"][0] > 0.5:
                                text = get_article_content(item_data["link"], source)
                                item_data["text"] = text
                                post_news_data(item_data, jwt, api_url)
                            elif result["scores"][0] > 0.5 and source is None:
                                post_news_data(item_data, jwt, api_url)
                            else:
                                print(f"News not relevant : {item_data['title']}")
                    else:
                        print(f"Language not supported: {lang}")
                except Exception as e:
                    print(f"An error occurred while posting news data: {e}")
        else:
            root = ET.fromstring(xml_content)
            for channel in root.findall(alert_array):
                for item in channel.findall(alert):
                    item_data = {}
                    item_data["flux_id"] = flux_id
                    for data_field in alert_data:
                        field_element = item.find(data_field)
                        if field_element is not None and field_element.text is not None:
                            item_data[data_field] = field_element.text
                        else:
                            item_data[data_field] = None
                    try:
                        lang = check_lang(item_data["title"])
                        if lang == "en" or lang == "fr":
                            print(item_data["link"])
                            if verify_news_is_not_already_posted(flux_id, item_data["title"], jwt, api_url):
                                sources = get_source_data(api_url, jwt)
                                source = check_url_source(sources, item_data["link"])
                                result = zero_shot_classify(item_data["title"], ["artificial intelligence", "machine learning", "deep learning", "neural networks", "ai", "software development", "programming", "coding", "web development", "software engineering", "developer", "code"])
                                if source is not None and result["scores"][0] > 0.5:
                                    text = get_article_content(item_data["link"], source)
                                    item_data["text"] = text
                                    post_news_data(item_data, jwt, api_url)
                                elif result["scores"][0] > 0.5 and source is None:
                                    post_news_data(item_data, jwt, api_url)
                                else:
                                    print(f"News not relevant : {item_data['title']}")
                        else:
                            print(f"Language not supported: {lang}")
                    except Exception as e:
                        print(f"An error occurred while posting news data: {e}")
    except Exception as e:
        print(f"An error occurred while parsing XML: {e}")

def zero_shot_classify(text, labels):
    classifier = pipeline('zero-shot-classification', model='joeddav/xlm-roberta-large-xnli')
    hypothesis_template = 'This text is about {}.'
    result = classifier(text, labels, hypothesis_template=hypothesis_template, multi_class=True)
    return result

def post_news_data(item_data, jwt, api_url):
    flux = item_data.get('flux_id', '')
    title = item_data.get('title', '')
    if not verify_news_is_not_already_posted(flux, title, jwt, api_url):
        print(f"News titled '{item_data.get('title', '')}' is already posted. Skipping.")
        return
    
    url = f"{api_url}/news/create"
    headers = {"Authorization": f"Bearer {jwt}"}
    query_params = {
        'flux_id': item_data.get('flux_id', ''),
        'title': item_data.get('title', ''),
        'text': item_data.get('text', item_data.get('content type', [''])),
        'description': item_data.get('description', ''),
        'sourceURL': item_data.get('sourceURL', item_data.get('link', '')),
        'pictureURL': item_data.get('pictureURL', ''),
        'type': item_data.get('type', 'rss')
    }
    try:
        response = requests.post(url , headers=headers, json=query_params)
        response.raise_for_status()
        if response.status_code == 201:
            print("News data posted successfully!")
        else:
            print(f"News data posting failed! Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while posting news data: {e}")
        
def verify_news_is_not_already_posted(flux_id, title, jwt, api_url):
    url = f"{api_url}/news/flux/{flux_id}"
    headers = {"Authorization": f"Bearer {jwt}"}
    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        if response.status_code == 200:
            news_data = response.json()
            news = news_data.get('data', [])
            if not news:
                return True
            if isinstance(news, list):
                for item in news:
                    if isinstance(item, dict):
                        if item.get("title") == title:
                            print('Title already posted')
                            return False
                    else:
                        print("Unexpected item format")
                        return False
                print(title)
                return True
            else:
                print("Unexpected response format")
                return False
        else:
            return False
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while fetching news data: {e}")
        return False

def get_article_content(url, source):
    response = requests.get(url)
    text = []
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        article_content = soup.find(source)
        if article_content:
            for element in article_content.descendants:
                if element.name == 'h1':
                    text.append(f"h1 : {element.get_text().strip()}\n")
                elif element.name == 'h2':
                    text.append(f"h2 : {element.get_text().strip()}\n")
                elif element.name == 'p':
                    textP = element.get_text().strip()
                    if textP and len(textP.split()) > 1 and "Sign up" not in textP and "Sign in" not in textP:
                        text.append(f"Paragraph : {textP}\n")
                elif element.name == 'ul' or element.name == 'ol':
                    for li in element.find_all('li'):
                        textLi = li.get_text().strip()
                        if textLi and len(textLi.split()) > 1:
                            text.append(f"List Item: {textLi}\n")
                elif element.name == 'img':
                    src = element.get('src')
                    text.append(f"Image : {src}\n")
                elif element.name == 'a' and 'href' in element.attrs:
                    source_url = urllib.parse.urljoin(url, element['href'])
                    text.append(f"Source : {source_url}\n")
            return text
        else:
            print("Failed to find the main article content.")
    else:
        print(f"Failed to retrieve the web page. Status code: {response.status_code}")

def check_lang(title):
    if title:
        lang = detect(title)
        return lang

def get_source_data(api_url, jwt):
    url = f"{api_url}/source"
    headers = {"Authorization": f"Bearer {jwt}"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        if response.status_code == 200:
            print("source data retrieved successfully!")
            sources = response.json()
            return sources
        else:
            print(f"sources data retrieval failed! Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while retrieving sources data: {e}")

def check_url_source(sources, url):
    for source in sources:
        if source['url'] in url:
            return source['main_div']
        else:
            print("Source not found.")
            return None
    return None

def main():
    username, password, api_url = load_env_variables()
    jwt = login(api_url, username, password)
    if jwt:
       flux = get_flux_data(api_url, jwt)
    if flux:
        get_news_data(flux, jwt, api_url)

if __name__ == "__main__":
    main()
